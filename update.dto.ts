import { AbstractDto, IAbstractDto } from './abstract.dto';
import { CampaignVcDto } from './campaign-vc.dto';
import { ProfileDto } from './profile.dto';

export interface IUpdateDto extends IAbstractDto {
    profile: ProfileDto;
    title: string;
    body: string;
    image: string;
    campaign: CampaignVcDto;
    site: any;
}

export class UpdateDto extends AbstractDto implements IUpdateDto {
    profile: ProfileDto;
    title: string;
    body: string;
    image: string;
    campaign: CampaignVcDto;
    site: any;
}