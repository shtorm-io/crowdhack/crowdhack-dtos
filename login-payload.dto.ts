import { TokenPayloadDto } from './token-payload.dto';
import { UserLoginResponseDto } from './user-login-response.dto';

export interface LoginPayloadDto {
  user: UserLoginResponseDto;
  token: TokenPayloadDto;
  site: string;
}
