import { RoleType } from './enums/role-type.enum';
import { IAbstractDto } from './abstract.dto';
import { ProfileDto } from './profile.dto';
import { SiteDto } from './site.dto';

export interface UserEntityDto extends IAbstractDto {
  role: RoleType;
  email: string;
  emailValidated: boolean;
  emailChallenge: string;
  emailChallengeExpiration: Date;
  phone: string;
  phoneValidated: boolean;
  phoneChallenge: boolean;
  phoneChallengeExpiration: Date;
  password: string;
  profile: ProfileDto;
  site: SiteDto;
}
