export declare enum SessionSourceEnum {
    CLOUDFLARE = "CLOUDFLARE",
    FIREBASE = "FIREBASE",
    CROWDHACK = "CROWDHACK",
    UNKNOWN = "UNKNOWN"
}
