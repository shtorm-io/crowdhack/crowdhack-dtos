export class UploadDto {
  path: string;
  filepath: string;
  id?: string;
  createdAt: string;
}
