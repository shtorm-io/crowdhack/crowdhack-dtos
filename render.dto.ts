export interface IRenderDtoOption {
    value: string | number;
    label: string;
}

export interface BaseItemRenderDtoClass {
    form?: string;
    input?: string;
    label?: string;
}

export interface IRenderDto {
    label: string;
    type: 'text' | 'select' | 'textarea' | 'checkbox' | 'date' | 'email' | 'file' | 'image' | 'number' | 'password' | 'url' | 'tel';
    validator?: string[];
    options?: IRenderDtoOption[];
    order?: number;
    required?: boolean;
    value?: number | string;
    placeholder?: string;
    class?: BaseItemRenderDtoClass;
    div?: string;
    mask?: string;
    small?: string;
    textarea?: BaseItemRenderDtoTextarea;
}

export interface BaseItemRenderDtoTextarea {
    cols?: number;
    rows?: number;
}

export class AbstractRenderDto {
    createdAt = null;
    updatedAt = null;
    id = null;
}
