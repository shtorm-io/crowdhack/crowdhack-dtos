import { Lazy } from './common/lazy';
import { AbstractDto, IAbstractDto } from './abstract.dto';
import { CampaignVcDto } from './campaign-vc.dto';
import { CommentVcDto } from './comment-vc.dto';
import { OrderDto } from './order.dto';
import { UpdateDto } from './update.dto';

export interface IProfileDto extends IAbstractDto {
    slug: string;
    name: string;
    title: string;
    body: string;
    avatar: string;
    campaigns: Lazy<CampaignVcDto[]>;
    orders: Lazy<OrderDto[]>;
    comments: Lazy<CommentVcDto[]>;
    zipCode: string;
    country: string;
    state: string;
    city: string;
    district: string;
    street: string;
    number: string;
    identity: string;
    updates: UpdateDto[];
}

export class ProfileDto extends AbstractDto implements IProfileDto {
    slug: string;
    name: string;
    title: string;
    body: string;
    avatar: string;
    campaigns: Lazy<CampaignVcDto[]>;
    orders: Lazy<OrderDto[]>;
    comments: Lazy<CommentVcDto[]>;
    zipCode: string;
    country: string;
    state: string;
    city: string;
    district: string;
    street: string;
    number: string;
    identity: string;
    updates: UpdateDto[];
}