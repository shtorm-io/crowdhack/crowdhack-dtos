import { IAbstractDto } from './abstract.dto';
import { UserEntityDto } from './user-entity.dto';

export interface UserMeResponseDto extends IAbstractDto {
  user: UserEntityDto;
  emptyPassword: boolean;
}