export interface IUserLoginDto {
    email: string;
    password: string;
}

export class UserLoginDto implements IUserLoginDto {
    email: string;
    password: string;

    constructor(email: string, password: string) {
        this.email = email;
        this.password = password;
    }
}