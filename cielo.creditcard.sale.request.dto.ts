export interface ICieloCreditcardSaleRequestDto {
    cardNumber: string;
    holder: string;
    expirationDate: string;
    securityCode: string;
    cardBrand: string;
    order: string;
    isRecurrency?: boolean;
    email?: string;
    name?: string;
}

export class CieloCreditcardSaleRequestDto implements ICieloCreditcardSaleRequestDto {
    cardNumber: string;
    holder: string;
    expirationDate: string;
    securityCode: string;
    cardBrand: string;
    order: string;
    isRecurrency?: boolean;
    email?: string;
    name?: string;
}
