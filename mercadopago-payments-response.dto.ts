import { AbstractDto } from './abstract.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class MercadopagoPaymentsResponseDto extends AbstractDto {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  email: string;

  @ApiProperty()
  @IsNumber()
  valor: number;

  @ApiProperty()
  @IsString()
  mission: string;

  @ApiProperty()
  @IsString()
  cpf: string;

  @ApiProperty()
  @IsString()
  data: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  created?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  operationType?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  status?: string;
}
