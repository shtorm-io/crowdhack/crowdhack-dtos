import { RoleType } from './enums/role-type.enum';
import { IAbstractDto } from './abstract.dto';

export interface UserLoginResponseDto extends IAbstractDto {
    role: RoleType;
    email: string;
    emailValidated: boolean;
    phone: string;
    phoneValidated: boolean;
}
