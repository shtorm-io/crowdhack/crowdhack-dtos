export enum CampaignVcFeatureStatusEnum {
    NORMAL = "NORMAL",
    FEATURED = "FEATURED",
    SPECIAL = "SPECIAL",
    HIDE = "HIDE",
    CHALLENGE = "CHALLENGE"
}