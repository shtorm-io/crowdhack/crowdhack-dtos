export enum MercadopagoWebhookTypeEnum {
    PAYMENT = 'payment',
    PLAN = 'plan',
    SUBSCRIPTION = 'subscription',
    INVOICE = 'invoice',
    UNKNOWN = 'unknown',
    TEST = 'test',
  }
  