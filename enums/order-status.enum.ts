export enum OrderStatus {
    // The user has not yet completed the payment process
    PENDING = 'pending',
    // The payment has been approved and accredited
    APPROVED = 'approved',
    // The payment has been authorized but not captured yet
    AUTHORIZED = 'authorized',
    // Payment is being reviewed
    INPROCESS = 'in_process',
    // Users have initiated a dispute
    INMEDIATION = 'in_mediation',
    // Payment was rejected. The user may retry payment.
    REJECTED = 'rejected',
    // Payment was cancelled by one of the parties or because time for payment has expired
    CANCELLED = 'cancelled',
    // Payment was refunded to the user
    REFUNDED = 'refunded',
    // Was made a chargeback in the buyer’s credit card
    CHARGEDBACK = 'charged_back',
    // Unprocessed payments
    UNPROCESSED = 'unprocessed',
  }
  