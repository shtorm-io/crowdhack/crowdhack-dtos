export enum PaymentGatewaySourceEnum {
    PAYPAL = 'PAYPAL',
    MERCADOPAGO = 'MERCADOPAGO',
    MANUAL = 'MANUAL',
    CIELO = 'CIELO',
    UNKNOWN = 'UNKNOWN',
}
