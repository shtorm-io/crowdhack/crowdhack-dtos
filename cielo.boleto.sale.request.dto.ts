export interface ICieloBoletoSaleRequestDto {
    zipCode: string;
    country: string;
    state: string;
    city: string;
    district: string;
    street: string;
    number: string;
    order: string;
    identity: string;
}
