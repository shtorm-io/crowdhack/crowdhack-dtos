export interface SiteListDto {
    accept_payment: boolean;
    accept_recurrence: boolean;
    id: string;
    slug: string;
}