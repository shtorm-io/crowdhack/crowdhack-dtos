import { IAbstractDto } from './abstract.dto';
import { CampaignVcDto } from './campaign-vc.dto';
import { OrderDto } from './order.dto';
import { ProfileDto } from './profile.dto';

export interface CommentVcDto extends IAbstractDto {
  comment: string;
  isAnonymous: boolean;
  campaign: CampaignVcDto;
  order: OrderDto;
  profile: ProfileDto;
}
