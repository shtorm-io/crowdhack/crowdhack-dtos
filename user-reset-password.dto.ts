export interface IUserResetPasswordDto {
    email: string;
    password: string;
    newPassword: string;
}
