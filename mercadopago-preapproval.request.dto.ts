export class MercadopagoPreapprovalRequestDto {
    site: string;
    campaign: string;
    price: number;
    name: string;
    email: string;
    comment: string;
    isAnonymous: boolean;
    code: string;
    phone: string;
    months: number;
}
