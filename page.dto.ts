import { IAbstractDto } from './abstract.dto';
import { ComponentDto } from './component.dto';
import { SiteDto } from './site.dto';

export interface PageDto extends IAbstractDto {
    name: string;
    path: string;
    type: string;
    component: ComponentDto;
    site: SiteDto;
    parameters: string;
}