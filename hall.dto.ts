import { AbstractDto, IAbstractDto } from './abstract.dto';
import { SiteDto } from './site.dto';
import { IRenderDto } from './render.dto';

export class HallDto extends AbstractDto {
    title: string | IRenderDto;
    image: string | IRenderDto;
    body: string | IRenderDto;
    slug: string | IRenderDto;
    site: SiteDto | IRenderDto;
}
