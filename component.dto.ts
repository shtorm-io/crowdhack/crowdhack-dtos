import { IAbstractDto } from './abstract.dto';
import { ComponentTypeColumnEnum } from './enums/component.enum';

export interface ComponentDto extends IAbstractDto {
  html: string;
  script: string;
  css: string;
  name: string;
  type: ComponentTypeColumnEnum;
  isPublished: boolean;
  isTheme: boolean;
  page: string;
}
