import { SessionSourceEnum } from './session-source.enum';

export interface IMercadopagoPreferenceIdRequestDto {
    campaignId: string;
    price: number;
    quantity: number;
    name: string;
    email: string;
    comment: string;
    isAnonymous: boolean;
    code: string;
    phone: string;
    sessionId: string;
    sessionSource: SessionSourceEnum;
}