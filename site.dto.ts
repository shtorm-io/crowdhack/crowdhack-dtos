import { IAbstractDto } from './abstract.dto';
import { CampaignVcDto } from './campaign-vc.dto';
import { ComponentDto } from './component.dto';
import { HallDto } from './hall.dto';
import { PageDto } from './page.dto';

export interface SiteDto extends IAbstractDto {
    accept_payment?: boolean;
    accept_recurrence?: boolean;
    slug?: string;
    page?: PageDto[];
    parameters?: string;
    script?: string;
    css?: string;
    component_header?: ComponentDto;
    component_footer?: ComponentDto;
    campaign?: CampaignVcDto[];
    hall?: HallDto[];
}