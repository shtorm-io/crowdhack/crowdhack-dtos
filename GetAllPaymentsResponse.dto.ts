import { AbstractDto } from './abstract.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNumber } from 'class-validator';
import { MercadopagoPaymentsResponseDto } from './mercadopago-payments-response.dto';

export class GetAllPaymentsResponseDto extends AbstractDto {
  @ApiProperty()
  @IsNumber()
  pagingTotal: number;

  @ApiProperty()
  @IsArray()
  res: MercadopagoPaymentsResponseDto[];
}
