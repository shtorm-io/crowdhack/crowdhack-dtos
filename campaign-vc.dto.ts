import { IAbstractDto } from './abstract.dto';
import { CampaignVcFeatureStatusEnum } from './enums/campaign-vc-feature-status.enum';
import { CommentVcDto } from './comment-vc.dto';
import { UpdateDto } from './update.dto';
import { SiteDto } from './site.dto';

export interface CampaignVcDto extends IAbstractDto {
  slug: string;
  title: string;
  summary: string;
  description: string;
  banner: string;
  currentMoney: number;
  currentHelpers: number;
  goalDate: string;
  goalMoney: number;
  whois: string;
  published: boolean;
  body: string;
  featuredStatus: CampaignVcFeatureStatusEnum;
  comments: CommentVcDto[];
  updates: UpdateDto[];
  site: SiteDto;
}
