export interface ICreatePasswordDto {
    password: string;
}

export class CreatePasswordDto implements ICreatePasswordDto {
    password: string;
}