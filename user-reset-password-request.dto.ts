export interface IUserResetPasswordRequestDto {
    email: string;
    password: string;
    token: string;
}

export class UserResetPasswordRequestDto implements IUserResetPasswordRequestDto {
    email: string;
    password: string;
    token: string;

    constructor(email: string, password: string, token: string) {
        this.email = email;
        this.password = password;
        this.token = token;
    }
}
