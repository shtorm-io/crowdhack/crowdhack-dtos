import { IAbstractDto } from './abstract.dto';
import { ComponentDto } from './component.dto';

export interface IThemeDto extends IAbstractDto {
    name: string;
    isPublic: boolean;
    isPublished: boolean;
    components: ComponentDto;
}