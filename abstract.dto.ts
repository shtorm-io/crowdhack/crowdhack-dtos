export interface IAbstractDto {
    id: string;
    createdAt: Date;
    updatedAt: Date;
}

export class AbstractDto implements IAbstractDto {
    id: string;
    createdAt: Date;
    updatedAt: Date;
}
