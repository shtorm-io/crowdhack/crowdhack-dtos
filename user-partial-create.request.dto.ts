import { IAbstractDto } from './abstract.dto';

export interface UserPartialCreateRequestDto extends IAbstractDto {
    name: string;
    email: string;
    phone: string;
    siteId: string;
    utmSource?: string;
    utmMedium?: string;
    utmCampaign?: string;
}
