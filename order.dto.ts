import { PaymentGatewaySourceEnum } from './enums/payment-gateway-source.enum';
import { MercadopagoWebhookTypeEnum } from './enums/mercadopago-webhook-type.enum';
import { OrderStatus } from './enums/order-status.enum';
import { CommentVcDto } from './comment-vc.dto';
import { IAbstractDto } from './abstract.dto';
import { Lazy } from './common/lazy';
import { ProfileDto } from './profile.dto';

export interface OrderDto extends IAbstractDto {
    externalId: string;
    gatewaySource: PaymentGatewaySourceEnum;
    orderStatus: OrderStatus;
    externalType: MercadopagoWebhookTypeEnum;
    comment: CommentVcDto;
    profile: Lazy<ProfileDto>;
    value: number;
}
