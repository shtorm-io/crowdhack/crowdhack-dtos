export interface IUserExistsDto {
    email: string;
    exists: boolean;
    passwordExist: boolean;
}

export class UserExistsDto implements IUserExistsDto {
    email: string;
    exists: boolean;
    passwordExist: boolean;
}
