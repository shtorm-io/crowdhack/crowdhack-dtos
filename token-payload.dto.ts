export interface TokenPayloadDto {
    expiresIn: number;
    accessToken: string;
}